from flask import Flask, render_template, url_for, request, redirect,jsonify,session

from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import cx_Oracle
import pandas as pd
from IPython.display import HTML
import os
import json

#ORACLE QUERY FOR TABLE



app = Flask(__name__)
#to remove warning message
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
#connect to SQL database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
app.secret_key = os.urandom(28)
db = SQLAlchemy(app)
#connect to local oracle
con = cx_Oracle.connect('system/testing12345@localhost:11521/XEPDB1')




@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        #convert all form posted data to JSON object
        result = json.dumps(request.form)
        final_result= json.loads(result) 
        
        #printing final JSON
        print ("final dictionary", str(final_result)) 
        print(type(final_result))

        #pass JSON object to /JSON
        session["data"] = final_result

        return redirect('/JSON')
        

    else:
        return render_template('index.html')



@app.route('/JSON', methods=['POST','GET'])
def test():
    #if POST REQUEST is successful
    if request.method=='POST':
        content = request.json
        #check values for all variables
        minPercent = content['minAltAllelePercent']
        minVariants = content['minVariants']
        start = content['start_date']
        end = content['end_date']
       


        #if INPUT value is not empty
        if start != "" and end != "":
            try:
                query = """SELECT library_type, trim(Count(*))
                            FROM
                            (SELECT library_type, accession 
                            FROM lowqualfiltered_small_variants
                            WHERE uploaded BETWEEN TO_DATE('%s', 'DD/MM/YYYY') AND TO_DATE('%s', 'DD/MM/YYYY') AND alt_percent >= '%s'
                            GROUP BY library_type, accession 
                            HAVING Count(*) > '%s')
                            GROUP BY library_type
                        """%(start,end,minPercent,minVariants)
                print(query)
                df_ora = pd.read_sql(query, con=con)
                print(df_ora)
                #set output path and file name
                df_ora.to_csv('./Table/result.csv')
                return render_template('simple.html',  tables=[df_ora.to_html(classes='data')], titles=df_ora.columns.values)
            except:
                return 'There was an issue with your parameters'
    
    
    #Accecpt posted JSON object from '/'
    dat= session["data"]
    

    #error check
    print('passed in data is',dat)
    print(type(dat))
    print(dat['minAltAllelePercent'])

    #check values for variables
    minPercent = dat['minAltAllelePercent']
    minVariants = dat['minVariants']
    start = dat['start_date']
    end = dat['end_date'] 
    if start != "" and end != "":
            try:
                query = """SELECT library_type, trim(Count(*))
                            FROM
                            (SELECT library_type, accession 
                            FROM lowqualfiltered_small_variants
                            WHERE uploaded BETWEEN TO_DATE('%s', 'DD/MM/YYYY') AND TO_DATE('%s', 'DD/MM/YYYY') AND alt_percent >= '%s'
                            GROUP BY library_type, accession 
                            HAVING Count(*) > '%s')
                            GROUP BY library_type
                        """%(start,end,minPercent,minVariants)
                print(query)
                df_ora = pd.read_sql(query, con=con)
                print(df_ora)
                #set output path and file name
                df_ora.to_csv('./Table/result.csv')
                return render_template('simple.html',  tables=[df_ora.to_html(classes='data')], titles=df_ora.columns.values)
            except:
                return 'There was an issue with your parameters'
    
    
        

    else:
        return "nope, gotta post somthing"


 

if __name__ == "__main__":
    app.run(debug=True)
