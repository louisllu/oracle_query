# ORACLE_query_API
A flask-oracle app used to query tables from ORACLE database(local)

## Usage

### Local Development
```
$ git clone...
$ cd <>
```
### requirement 
Click==7.0  
Flask==1.0.3  
Flask-SQLAlchemy==2.4.0  
gunicorn==19.9.0  
itsdangerous==1.1.0  
Jinja2==2.10.1  
MarkupSafe==1.1.1  
SQLAlchemy==1.3.3  
Werkzeug==0.15.4  
pandas==1.1.3  
cx_Oracle==8.0.1


### Setting up virtual environment
In the top level of parquet_api, run:

```
$ virtualenv -p python3 venv
$ source ./venv/bin/activate # to activate virtual environment(source ./venv/bin/activate.fish if using fish command line)
$ pip install -r requirements.txt
```

#### Make sure to activate the virtual environment for all the following steps. To deactivate, simply run `deactivate`.

### Server
In the top level of parquet_api, run:

```
$ gunicorn app:app
```

### How to use

#### 1.Query table based on inputs:

Input values for 4 variables and the API will return a JSON object and make ORACLE query from the database and return result table


## Resources

1. [static files in flask](https://stackoverflow.com/questions/20646822/how-to-serve-static-files-in-flask)

2. [python3 virtualenv](https://stackoverflow.com/questions/23842713/using-python-3-in-virtualenv)
